package ie.wit.slideshowgenerator;

import java.util.ArrayList;
import java.util.List;
import java.io.Serializable;

/**
 * Stores the data for an individual slideshow in the
 * <em>Slideshow Generator</em> app.
 * 
 * <p>
 * <code>SlideshowInformation</code> objects are stored using object
 * serialization. 
 * 
 * @author Donal McGee
 */

public class SlideshowInformation implements Serializable
{
  /**
   * Represents the <code>class</code>'s version.
   */
  private static final long serialVersionUID = 1L;

  /**
   * Represents the name of the slideshow that will be displayed in the
   * slideshow List.
   */
  private String name;

  /**
   * Represents the locations of images and videos and the type of each item.
   */
  private List<MediaItem> mediaItemList;

  /**
   * Represents the location of the music that will play in the background of
   * the slideshow.
   */
  private String slideshowMusicPath;

  /**
   * The SlideshowInfoActivity Constructor:
   * <p>
   * <ul>
   * <li>Sets the name of the slideshow to <code>slideshowName</code>.
   * <li>Creates <code>mediaItemList</code> as an
   * <code>ArrayList&lt;MediaItem&gt;</code>.
   * <li>Sets the <code>slideshowMusicPath</code> to <code>null</code>.
   * </ul>
   * <p>
   * 
   * @param slideshowName
   *          The name of the slideshow.
   */
  public SlideshowInformation(String slideshowName)
  {
    name = slideshowName;
    mediaItemList = new ArrayList<MediaItem>();
    slideshowMusicPath = null;
  }

  /**
   * Returns the <code>name</code> of the slideshow.
   * <p>
   * 
   * @return <code>name</code>
   */
  public String getName()
  {
    return name;
  }

  /**
   * Returns a list of MediaItems that points to the images and videos in a
   * slideshow.
   * <p>
   * 
   * @return <code>mediaItemList</code>
   */
  public List<MediaItem> getMediaItemList()
  {
    return mediaItemList;
  }

  /**
   * Adds a new Media Item.
   * <p>
   * 
   * @param type
   *          Specifies whether the <code>MediaItem</code> represents an image
   *          or a video.
   * 
   * @param path
   *          An image <code>path</code>.
   */
  public void addMediaItem(MediaItem.MediaType type, String path)
  {
    mediaItemList.add(new MediaItem(type, path));
  }

  /**
   * Returns a <code>String</code> at position <code>index</code>.
   * <p>
   * 
   * @param index
   *          Position where the <code>mediaItemList</code> will be returned to.
   * @return <code>mediaItemList.get(index)</code>, <code>null</code>
   */
  public MediaItem getMediaItemAt(int index)
  {
    if (index >= 0 && index < mediaItemList.size())
    {
      return mediaItemList.get(index);
    }
    else
      return null;
  }

  /**
   * Returns this slideshow's music.
   * <p>
   * 
   * @return <code>slideshowMusicPath</code>
   */
  public String getSlideshowMusicPath()
  {
    return slideshowMusicPath;
  }

  /**
   * Sets this slideshow's music.
   * <p>
   * 
   * @param path
   *          The path to the slideshow's music.
   */
  public void setSlideshowMusicPath(String path)
  {
    slideshowMusicPath = path;
  }

  /**
   * Returns the number of images and videos in the slideshow.
   * <p>
   * 
   * @return <code>slideshowImageList.size()</code>
   */
  public int size()
  {
    return mediaItemList.size();
  }
}