package ie.wit.slideshowgenerator;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore.Images;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Toast;

/**
 * Takes a picture with the Android device's camera and automatically adds it to
 * both the Gallery app and the slideshow itself. Author: Donal McGee
 */

public class SlideshowTakePicture extends Activity
{
  /**
   * Logs errors.
   */
  private static final String TAG = "PICTURE_TAKER";

  /**
   * Displays the camera preview.
   */
  private SurfaceView surfaceView;

  /**
   * Manages the SurfaceView changes.
   */
  private SurfaceHolder surfaceHolder;

  /**
   * Checks that the preview running.
   */
  private boolean isPreviewing;

  /**
   * Used to capture image data.
   */
  private Camera camera;

  /**
   * Supports preview sizes of camera.
   */
  private List<Camera.Size> sizes;

  @SuppressWarnings("deprecation")
  @Override
  public void onCreate(Bundle bundle)
  {
    super.onCreate(bundle);
    setContentView(R.layout.activity_slideshow_camera);

    /**
     * Initialises the surfaceView and sets its touch listener.
     */
    surfaceView = (SurfaceView) findViewById(R.id.cameraSurfaceView);
    surfaceView.setOnTouchListener(touchListener);

    /**
     * Initialises the surfaceHolder and sets the object to handle its
     * callbacks.
     */
    surfaceHolder = surfaceView.getHolder();
    surfaceHolder.addCallback(surfaceCallback);

    /**
     * Required before Android 3.0 for camera preview functionality.
     */
    surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
  }

  /**
   * Handles the SurfaceHolder.Callback events.
   */
  private SurfaceHolder.Callback surfaceCallback = new SurfaceHolder.Callback()
  {
    /**
     * Releases the resources after the SurfaceView is destroyed.
     */
    @Override
    public void surfaceDestroyed(SurfaceHolder arg0)
    {
      camera.stopPreview();
      isPreviewing = false;
      camera.release();
    }

    /**
     * Initializes the camera when the SurfaceView is created and its supported
     * color effects / preview sizes.
     */
    @Override
    public void surfaceCreated(SurfaceHolder arg0)
    {
      camera = Camera.open();
      sizes = camera.getParameters().getSupportedPreviewSizes();
    }

    /**
     * Called every time the <code>SurfaceView</code> changes - when the
     * <code>SurfaceView</code> is first created and when the device is rotated.
     */
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
    {
      if (isPreviewing)
        camera.stopPreview();

      /**
       * Configures and sets the camera parameters.
       */
      Camera.Parameters p = camera.getParameters();
      p.setPreviewSize(sizes.get(0).width, sizes.get(0).height);
      camera.setParameters(p);

      try
      {
        camera.setPreviewDisplay(holder);
      }
      catch (IOException e)
      {
        Log.v(TAG, e.toString());
      }

      camera.startPreview();
      isPreviewing = true;
    }
  };

  /**
   * Handles Camera callbacks.
   * 
   * <p>
   * When the user takes a picture, this class receives the image data.
   */
  Camera.PictureCallback pictureCallback = new Camera.PictureCallback()
  {
    public void onPictureTaken(byte[] imageData, Camera c)
    {
      /**
       * Uses "SlideshowGenerator_" + current time in milliseconds as new image
       * file name.
       */
      String fileName = "SlideshowGenerator_" + System.currentTimeMillis();

      /**
       * Creates a ContentValues and configures the new image's data.
       */
      ContentValues values = new ContentValues();
      values.put(Images.Media.TITLE, fileName);
      values.put(Images.Media.DATE_ADDED, System.currentTimeMillis());
      values.put(Images.Media.MIME_TYPE, "image/jpg");

      Uri uri = getContentResolver().insert(Images.Media.EXTERNAL_CONTENT_URI, values);

      try
      {
        /**
         * Gets an OutputStream to uri.
         */
        OutputStream outStream = getContentResolver().openOutputStream(uri);
        outStream.write(imageData);
        outStream.flush();
        outStream.close();

        /**
         * <code>Intent</code> for returning data to SlideshowEditor
         */
        Intent returnIntent = new Intent();
        returnIntent.setData(uri);
        setResult(RESULT_OK, returnIntent);

        /**
         * Display a message indicating that the image was saved to the Gallery
         * app.
         */
        Toast message = Toast.makeText(SlideshowTakePicture.this, R.string.message_saved, Toast.LENGTH_SHORT);
        message.setGravity(Gravity.CENTER, message.getXOffset() / 2, message.getYOffset() / 2);
        message.show();

        finish();
      }
      /**
       * Catches an error when taking a picture.
       */
      catch (IOException ex)
      {
        setResult(RESULT_CANCELED);

        Toast message = Toast.makeText(SlideshowTakePicture.this, R.string.message_error_saving, Toast.LENGTH_SHORT);
        message.setGravity(Gravity.CENTER, message.getXOffset() / 2, message.getYOffset() / 2);
        message.show();
      }
    }
  };

  /**
   * Takes a picture when the user touches the screen.
   */
  private OnTouchListener touchListener = new OnTouchListener()
  {
    @Override
    public boolean onTouch(View v, MotionEvent event)
    {
      camera.takePicture(null, null, pictureCallback);
      return false;
    }
  };
}