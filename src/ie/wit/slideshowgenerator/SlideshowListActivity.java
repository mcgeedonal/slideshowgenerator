package ie.wit.slideshowgenerator;

import java.util.ArrayList;
import java.util.List;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * The main <code>Activity</code> <code>class</code> for
 * <em>Slideshow Generator</em>.
 * 
 * <p>
 * Extends <code>ListActivity</code> in order to display a <code>ListView</code>
 * of all the previously created and saved slideshows.
 * 
 * @author Donal McGee
 * 
 */
public class SlideshowListActivity extends ListActivity
{
  /**
   * Adds an error-logging tag.
   */
  private static final String TAG = "SLIDESHOW";

  /**
   * Adds a slideshow name as an <code>Extra</code> to an <code>Intent</code>.
   */
  public static final String NAME_INTENT_EXTRA = "NAME";

  /**
   * Represents a List of <code>SlideshowInformation Objects</code> that contain
   * the information for all slideshows.
   * 
   * <p>
   * It is declared as static so it can be shared among
   * <em>Slideshow Generator</em>'s other Activities.
   */
  static List<SlideshowInformation> slideshowList;

  /**
   * This <code>ListActivity<code>'s <code>ListView</code>.
   */
  private ListView slideshowListView;

  /**
   * Displays objects from <code>SlideshowInformation</code> as items in a
   * <code>ListView</code>.
   */
  private SlideshowAdapter slideshowAdapter;

  /**
   * Represents the location of the slideshows on the device.
   */
  private File slideshowFile;

  /**
   * Creates a <code>File</code> object representing the location where
   * <em>Slideshow Generator</em> stores slideshows on the Android device.
   * 
   * <p>
   * Gets the location of the <code>File</code> and starts the task that loads
   * the slideshows.
   */
  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    /**
     * Gets the built-in <code>ListView</code> for
     * <code>slideshowListView</code>.
     */
    super.onCreate(savedInstanceState);
    slideshowListView = getListView();

    /**
     * The method <code>getExternalFilesDir</code> returns a <code>File</code>
     * representing an external storage directory on the device, i.e., an SD
     * card.
     * <p>
     * It calls <code>getAbsolutePath</code> on the <code>File</code> object,
     * then appends <code>/SlideshowGenerator.ser</code> to create a path to the
     * file in which <em>Slideshow Generator</em> will store its slideshows.
     */
    slideshowFile = new File(getExternalFilesDir(null).getAbsolutePath() + "/SlideshowGenerator.ser");

    /**
     * Creates an object of the AsyncTask subclass
     * <code>LoadSlideshowsTask</code> and calls its <code>execute</code> method
     * to load previously saved slideshows.
     * 
     * @param null The task has no arguments so null is passed.
     */
    new LoadSlideshowsTask().execute((Object[]) null);

    /**
     * Creates an <code>AlertDialog Builder</code> that welcomes the user and
     * explains to them how to begin using <em>Slideshow Generator</em>.
     * <p>
     */

    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setTitle(R.string.welcome_message_title);
    builder.setMessage(R.string.welcome_message);
    builder.setPositiveButton(R.string.button_ok, null);
    builder.show();
  }

  /**
   * Loads the List<SlideshowInformation> object from the device.
   * 
   * @author Donal McGee
   */
  private class LoadSlideshowsTask extends AsyncTask<Object, Object, Object>
  {
    /**
     * Checks whether the SlideshowGenerator.ser file exists.
     * 
     * <p>
     * If it does exist, an <code>ObjectInputStream</code> is created.
     * 
     * The method <code>readObject</code> is called and it reads the
     * <code>List&lt;SlideshowInformation&gt;</code> object from the
     * <code>slideshowFile</code>.
     * 
     * <p>
     * If the file does not exist, a new
     * <code>List&lt;SlideshowInformation&gt;</code> object is created.
     * 
     * <p>
     * 
     * @throws Exception
     *           If an Exception occurs, the method <code>runOnUiThread</code>
     *           displays a toast from the UI thread to alert the user.
     */
    @Override
    protected Object doInBackground(Object... arg0)
    {
      if (slideshowFile.exists())
      {
        try
        {
          ObjectInputStream input = new ObjectInputStream(new FileInputStream(slideshowFile));
          slideshowList = (List<SlideshowInformation>) input.readObject();
        }
        catch (final Exception e)
        {
          runOnUiThread(new Runnable()
          {
            public void run()
            {
              Toast message = Toast.makeText(SlideshowListActivity.this, R.string.message_error_reading,
                  Toast.LENGTH_LONG);
              message.setGravity(Gravity.CENTER, message.getXOffset() / 2, message.getYOffset() / 2);
              message.show();
              Log.v(TAG, e.toString());
            }
          });
        }
      }

      if (slideshowList == null)
        slideshowList = new ArrayList<SlideshowInformation>();

      return (Object) null;
    }

    /**
     * Sets up the SlideshowListActivity's ListView adapter on the UI thread
     * when the background task completes.
     */
    @Override
    protected void onPostExecute(Object result)
    {
      super.onPostExecute(result);

      slideshowAdapter = new SlideshowAdapter(SlideshowListActivity.this, slideshowList);
      slideshowListView.setAdapter(slideshowAdapter);
    }
  }

  /**
   * Serializes the <code>List&lt;SlideshowInformation&gt;</code> object to a
   * file.
   */
  private class SaveSlideshowsTask extends AsyncTask<Object, Object, Object>
  {
    /**
     * Checks whether the SlideshowGenerator.ser file exists, and if it doesn't,
     * it creates the file.
     * 
     * <p>
     * 
     * An ObjectOutputStream is created and it calls writeObject to write the
     * <code>List&lt;SlideshowInformation&gt;</code> object into
     * <code>slideshowFile</code>.
     * 
     * <p>
     * 
     * @throws Exception
     *           If an Exception occurs, the method <code>runOnUiThread</code>
     *           displays a toast from the UI thread to alert the user.
     * 
     *           <p>
     * @return null
     */
    @Override
    protected Object doInBackground(Object... arg0)
    {
      try
      {
        if (!slideshowFile.exists())
          slideshowFile.createNewFile();

        ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(slideshowFile));
        output.writeObject(slideshowList);
        output.close();
      }
      catch (final Exception e)
      {
        runOnUiThread(new Runnable()
        {
          public void run()
          {
            Toast message = Toast.makeText(SlideshowListActivity.this, R.string.message_error_writing,
                Toast.LENGTH_LONG);
            message.setGravity(Gravity.CENTER, message.getXOffset() / 2, message.getYOffset() / 2);
            message.show();
            Log.v(TAG, e.toString());
          }
        });
      }

      return (Object) null;
    }
  }

  /**
   * onCreateOptionsMenu inflates the Activity's menu from
   * <code>slideshow_generator_menu</code>.
   * 
   * @return true
   * @param menu
   *          The options menu in which the menu item is placed.
   */
  @Override
  public boolean onCreateOptionsMenu(Menu menu)
  {
    super.onCreateOptionsMenu(menu);
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.slideshow_generator_menu, menu);
    return true;
  }

  /**
   * A request code from SlideshowEditorActivity that is passed to
   * startActivityForResult.
   */
  private static final int EDIT_ID = 0;

  /**
   * onOptionsItemSelected handles the single menu item from the
   * <em>SlideshowGenerator</em> Options menu.
   * <p>
   * When the user touches the <i>Create New Slideshow</i> menu item, this
   * method displays a dialog in which the user can enter the Slideshow�s name.
   * <p>
   * The <code>LAYOUT_INFLATER_SERVICE</code> is used with
   * <code>getSystemService</code> to retrieve a <code>LayoutInflater</code> for
   * the file <code>activity_slideshow_name.xml</code>, which in turn creates an
   * <code>EditText</code>.
   * <p>
   * The <code>EditText</code> is displayed in the dialog and it is set as the
   * View.
   * <p>
   * An <code>AlertBuilder</code> creates an input dialog box that allows the
   * user to name their newly-created Slideshow.
   */
  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    View view = inflater.inflate(R.layout.activity_slideshow_name, null);
    final EditText slideshowNameEditText = (EditText) view.findViewById(R.id.slideshowNameEditText);

    AlertDialog.Builder inputDialog = new AlertDialog.Builder(this);
    inputDialog.setView(view);
    inputDialog.setTitle(R.string.dialog_name_title);

    /**
     * If the user touches the Save button in the dialog box, the method onClick
     * gets the slideshow name from the EditText.
     * 
     * <p>
     * It then creates a new SlideshowInfoActivity object for the Slideshow and
     * adds it to the slideshowList.
     * 
     * <p>
     * An Intent is created to launch the SlideshowEditorActivity class.
     * 
     * <p>
     * This is achieved using the startActivityForResult method.
     * 
     * <p>
     * The slideshow name is added as an Extra before the Activity is started.
     * 
     * @param editSlideshowIntent
     *          The Intent representing the sub-Activity to launch.
     * @param EDIT_ID
     *          A non-negative request code that identifies which Activity is
     *          returning a result.
     */
    inputDialog.setPositiveButton(R.string.button_slideshow_name, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface dialog, int whichButton)
      {
        String name = slideshowNameEditText.getText().toString().trim();

        if (name.length() != 0)
        {
          slideshowList.add(new SlideshowInformation(name));

          Intent editSlideshowIntent = new Intent(SlideshowListActivity.this, SlideshowEditorActivity.class);
          editSlideshowIntent.putExtra(NAME_INTENT_EXTRA, name);
          startActivityForResult(editSlideshowIntent, EDIT_ID);
        }

        /**
         * The Toast object displays a message to the user that a Slideshow must
         * have a name.
         */

        else
        {
          Toast message = Toast.makeText(SlideshowListActivity.this, R.string.message_name_error, Toast.LENGTH_SHORT);
          message.setGravity(Gravity.CENTER, message.getXOffset() / 2, message.getYOffset() / 2);
          message.show();
        }
      }
    });

    inputDialog.setNegativeButton(R.string.button_cancel, null);
    inputDialog.show();

    return super.onOptionsItemSelected(item);
  }

  /**
   * Saves the <code>List&lt;SlideshowInformation&gt;</code> object once the
   * user has completed editing a slideshow.
   * 
   * <p>
   * Creates an object of the <code>AsyncTask</code> subclass
   * <code>SaveSlideshowTask</code> and invokes its <code>execute</code> method.
   * 
   * <p>
   * SlideshowAdapter�s notifyDataSetChanged method is called to indicate that
   * the adapter�s underlying data set has changed and the ListView is
   * refreshed.
   * 
   * <p>
   * 
   * @param requestCode
   *          EDIT_ID (the second parameter in startActivityForResult) is passed
   *          here as the first parameter in this method.
   * @param resultCode
   *          Has a value of either RESULT_OK (if the Activity completed
   *          successfully) or RESULT_CANCELED (if the Activity did not return a
   *          result)
   * @param data
   *          An Intent containing data returned to this Activity.
   */

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data)
  {
    super.onActivityResult(requestCode, resultCode, data);
    new SaveSlideshowsTask().execute((Object[]) null);
    slideshowAdapter.notifyDataSetChanged();
  }

  /**
   * Implements the Viewholder pattern for better ListView performance.
   * 
   * <p>
   * Defines package-access instance variables that the SlideshowAdapter class
   * will be able to access when manipulating ViewHolder objects.
   * 
   * <p>
   * Whenever a ListView item is created, an object of class ViewHolder is also
   * created and associated with that ListView item.
   * 
   * <p>
   * If there is an existing ListView item that's being reused, then the
   * ViewHolder object that was previously associated with that item is
   * obtained.
   */
  private static class ViewHolder
  {
    /**
     * References the ListView item's TextView.
     */
    TextView slideshowNameTextView;

    /**
     * References the ListView item's ImageView.
     */
    ImageView slideshowImageView;

    /**
     * References the ListView item's Play Button.
     */
    Button playButton;

    /**
     * References the ListView item's Edit Button.
     */
    Button editButton;

    /**
     * References the ListView item's Delete Button.
     */
    Button deleteButton;
  }

  /**
   * Displays a slideshow's name, cover image and <i>Play</i>, <i>Edit</i> and
   * <i>Delete<i> Buttons.
   * 
   * <p>
   * The class ArrayAdapter is extended so that the method getView can be
   * overridden.
   * 
   * <p>
   * This is done to configure a custom layout for each ListView item.
   */

  private class SlideshowAdapter extends ArrayAdapter<SlideshowInformation>
  {
    private List<SlideshowInformation> items;
    private LayoutInflater inflater;

    /**
     * Calls the superclass�s constructor, then stores the List of
     * SlideshowInfoActivity objects and the LayoutInflater for use in the
     * getView method.
     * 
     * <p>
     * The second superclass constructor argument represents the resource ID of
     * a layout that contains a TextView for displaying data in a ListView item.
     * 
     * <p>
     * -1 is supplied for that argument.
     * 
     * @param context
     *          Context
     * @param items
     *          Items
     */
    public SlideshowAdapter(Context context, List<SlideshowInformation> items)
    {
      super(context, -1, items);
      this.items = items;
      inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     * Performs mapping of data to a ListView item.
     * 
     * @param position
     *          The ListView item's position.
     * @param convertView
     *          The ListView item's contents.
     *          <p>
     * @param parent
     *          The ListView item's parents.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
      /**
       * The ViewHolder object is then set as ListView item's tag.
       */
      ViewHolder viewHolder;

      /**
       * If convertView is null, activity_slideshow_list.xml is inflated and
       * assigned to a convertView.
       * 
       * <p>
       * A ViewHolder object is then created and the GUI components that were
       * just inflated are assigned to the ViewHolder's instance variables.
       * 
       * <p>
       * The ViewHolder object is set as the ListView item's tag.
       */
      if (convertView == null)
      {
        convertView = inflater.inflate(R.layout.activity_slideshow_list, null);

        viewHolder = new ViewHolder();
        viewHolder.slideshowNameTextView = (TextView) convertView.findViewById(R.id.slideshowNameTextView);
        viewHolder.slideshowImageView = (ImageView) convertView.findViewById(R.id.slideshowImageView);
        viewHolder.playButton = (Button) convertView.findViewById(R.id.playButton);
        viewHolder.editButton = (Button) convertView.findViewById(R.id.editButton);
        viewHolder.deleteButton = (Button) convertView.findViewById(R.id.deleteButton);
        convertView.setTag(viewHolder);
      }

      /**
       * If convertView is not null, the ListView is reusing a ListView item
       * that has scrolled off the screen.
       * 
       * <p>
       * The tag of the ListView item is retrieved and the ViewHolder object is
       * reused.
       * 
       * <p>
       * convertView gets the SlideshowInfoActivity object that corresponds to
       * the ListView item�s position.
       */
      else
        viewHolder = (ViewHolder) convertView.getTag();

      SlideshowInformation slideshowInformation = items.get(position);
      /**
       * Get the Slideshow the display its name in slideshowNameTextView.
       */
      viewHolder.slideshowNameTextView.setText(slideshowInformation.getName());

      /**
       * If there is at least one item in this slideshow, a <code>Bitmap</code>
       * is created using the first image or video in the slideshow.
       */
      if (slideshowInformation.size() > 0)
      {
        MediaItem firstItem = slideshowInformation.getMediaItemAt(0);
        new LoadThumbnailTask().execute(viewHolder.slideshowImageView, firstItem.getType(),
            Uri.parse(firstItem.getPath()));
      }

      /**
       * Configures the listeners for the Play, Edit and Delete buttons in this
       * ListView item.
       */
      viewHolder.playButton.setTag(slideshowInformation);
      viewHolder.playButton.setOnClickListener(playButtonListener);

      viewHolder.editButton.setTag(slideshowInformation);
      viewHolder.editButton.setOnClickListener(editButtonListener);

      viewHolder.deleteButton.setTag(slideshowInformation);
      viewHolder.deleteButton.setOnClickListener(deleteButtonListener);

      return convertView;
    }
  }

  /**
   * Loads thumbnails in a separate thread of execution to ensure the GUI stays
   * repsonsive.
   * 
   */
  private class LoadThumbnailTask extends AsyncTask<Object, Void, Bitmap>
  {
    /**
     * Displays the thumbnail.
     */
    ImageView slideshowImageView;

    /**
     * Uses the method <code>getThumbnail</code> to load the thumbnail.
     */
    @Override
    protected Bitmap doInBackground(Object... params)
    {
      slideshowImageView = (ImageView) params[0];

      return SlideshowListActivity.getThumbnail((MediaItem.MediaType) params[1], (Uri) params[2], getContentResolver(),
          new BitmapFactory.Options());
    }

    /**
     * Receives the thumbnail <code>Bitmap</code> and displays it on the
     * specified <code>ImageView</code>.
     */
    @Override
    protected void onPostExecute(Bitmap result)
    {
      super.onPostExecute(result);
      slideshowImageView.setImageBitmap(result);
    }
  }

  /**
   * Responds to events generated by the "Play" Button.
   */
  OnClickListener playButtonListener = new OnClickListener()
  {
    @Override
    public void onClick(View v)
    {
      /**
       * Creates an intent to launch the SlideshowPlayerActivity.
       */
      Intent playSlideshow = new Intent(SlideshowListActivity.this, SlideshowPlayerActivity.class);
      /**
       * Adds the slideshow's name as an <code>Intent</code> extra.
       */
      playSlideshow.putExtra(NAME_INTENT_EXTRA, ((SlideshowInformation) v.getTag()).getName());
      startActivity(playSlideshow);
    }
  };

  /**
   * Responds to events generated by the "Edit" Button.
   */
  private OnClickListener editButtonListener = new OnClickListener()
  {
    @Override
    public void onClick(View v)
    {
      /**
       * Creates an intent to launch the SlideshowEditorActivity.
       */
      Intent editSlideshowActivity = new Intent(SlideshowListActivity.this, SlideshowEditorActivity.class);
      /**
       * Adds the slideshow's name as an <code>Intent</code> extra.
       */
      editSlideshowActivity.putExtra(NAME_INTENT_EXTRA, ((SlideshowInformation) v.getTag()).getName());
      startActivityForResult(editSlideshowActivity, 0);
    }
  };

  /**
   * Responds to events generated by the "Delete" Button.
   */
  private OnClickListener deleteButtonListener = new OnClickListener()
  {
    @Override
    public void onClick(final View v)
    {
      /**
       * Confirmation that the user wants to delete the slideshow.
       */
      AlertDialog.Builder builder = new AlertDialog.Builder(SlideshowListActivity.this);
      builder.setTitle(R.string.dialog_confirm_delete);
      builder.setMessage(R.string.dialog_confirm_delete_message);

      builder.setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener()
      {
        /**
         * <code>getTag</code> method retrieves the
         * <code>SlideshowInformation</code> <code>object</code> that was set
         * with <code>setTag</code> and then removes that object.
         */
        @Override
        public void onClick(DialogInterface dialog, int which)
        {
          SlideshowListActivity.slideshowList.remove((SlideshowInformation) v.getTag());
          new SaveSlideshowsTask().execute((Void) null);
          /**
           * Refreshes the ListView.
           */
          slideshowAdapter.notifyDataSetChanged();
        }
      });
      builder.setNegativeButton(R.string.button_cancel, null);
      builder.show();
    }
  };

  /**
   * Returns a SlideshowInformation object.
   * 
   * @param name
   * @return slideshowInformation null
   */
  public static SlideshowInformation getSlideshowInformation(String name)
  {
    /**
     * Iterates through the list of <code>SlideshowInformation</code> objects
     * and compares the stored slideshow name with the <code>name</code>.
     * 
     * <p>
     * Returns the <code>SlideshowInformation</code> if found.
     * 
     * <p>
     * Returns null, if not found.
     */
    for (SlideshowInformation slideshowInformation : slideshowList)
    {
      if (slideshowInformation.getName().equals(name))
        return slideshowInformation;
    }

    return null;
  }

  /**
   * Loads thumbnails for both images and videos.
   * 
   * <p>
   * 
   * @param type
   * 
   * @param uri
   *          Represents the location of the image.
   * @param cr
   *          Interacts with the Android device's files system.
   * @param options
   *          Specifies the Bitmap configuration.
   * @return bitmap
   */
  public static Bitmap getThumbnail(MediaItem.MediaType type, Uri uri, ContentResolver cr, BitmapFactory.Options options)
  {
    Bitmap bitmap = null;
    /**
     * Extracts the image or video id from the Uri
     */
    int id = Integer.parseInt(uri.getLastPathSegment());

    /**
     * Uses the Android MediaStore to get the corresponding thumbnail image.
     */

    if (type == MediaItem.MediaType.IMAGE)
      bitmap = MediaStore.Images.Thumbnails.getThumbnail(cr, id, MediaStore.Images.Thumbnails.MICRO_KIND, options);
    else
      if (type == MediaItem.MediaType.VIDEO)
        bitmap = MediaStore.Video.Thumbnails.getThumbnail(cr, id, MediaStore.Video.Thumbnails.MICRO_KIND, options);

    return bitmap;
  }
}