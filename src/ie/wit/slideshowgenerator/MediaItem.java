package ie.wit.slideshowgenerator;

import java.io.Serializable;

/**
 * Used to represent images and videos in a slideshow.
 * 
 * <p>
 * 
 * @author Donal McGee
 */

public class MediaItem implements Serializable
{
  /**
   * Represents the <code>class</code>'s version and is used during
   * deserialization.
   * 
   * <p>
   * Keeps track of different versions of a class in order to perform valid
   * serialization of objects.
   */
  private static final long serialVersionUID = 1L;

  /**
   * Represents the constants for the two different media types:
   * <code>IMAGE</code> and <code>VIDEO</code>
   */
  public static enum MediaType
  {
    IMAGE, VIDEO
  }

  /**
   * Represents the type of <code>MediaItem</code>; <code>IMAGE</code> or
   * <code>VIDEO</code>.
   */
  private MediaType type;

  /**
   * Represents the location of this MediaItem.
   */
  private String path;

  /**
   * The MediaItem Constructor:
   * <p>
   * <ul>
   * <li>Sets the <code>type</code> of the <code>MediaItem</code> to
   * <code>mediaType</code>.
   * <li>Sets the <code>path</code> of the <code>MediaItem</code> to
   * <code>location</code>.
   * </ul>
   * <p>
   * 
   * @param mediaType
   *          The type of MediaItem: IMAGE or VIDEO.
   * @param location
   *          The location of this MediaItem.
   */
  public MediaItem(MediaType mediaType, String location)
  {
    type = mediaType;
    path = location;
  }

  /**
   * Gets the MediaType of the image or video.
   * 
   * @return type
   */
  public MediaType getType()
  {
    return type;
  }

  /**
   * Returns the location of the image of video.
   * 
   * @return path
   */
  public String getPath()
  {
    return path;
  }
}