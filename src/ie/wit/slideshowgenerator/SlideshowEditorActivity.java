package ie.wit.slideshowgenerator;

import java.util.List;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;

/**
 * The <code>Activity</code> <code>class</code> for editing and building a
 * slideshow.
 * 
 * <p>
 * Author: Donal McGee
 */

public class SlideshowEditorActivity extends ListActivity
{
  /**
   * Displays the images or videos of the slideshow being edited in the
   * ListView.
   */
  private SlideshowEditorAdapter slideshowEditorAdapter;
  private SlideshowInformation slideshowInformation;

  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);

    /**
     * Sets the ListActivity's layout to the layout specified in
     * <code>activity_slideshow_editor.xml</code>
     */
    setContentView(R.layout.activity_slideshow_editor);

    /**
     * Gets the <code>Intent</code> that launched the Activity, then gets the
     * <code>String</code> extra that was stored in the Intent's bundle.
     */
    String name = getIntent().getStringExtra(SlideshowListActivity.NAME_INTENT_EXTRA);
    slideshowInformation = SlideshowListActivity.getSlideshowInformation(name);

    /**
     * Sets the appropriate <code>onClickListener</code> for each Button.
     */
    Button addPictureButton = (Button) findViewById(R.id.addPictureButton);
    addPictureButton.setOnClickListener(addPictureButtonListener);

    Button takePictureButton = (Button) findViewById(R.id.takePictureButton);
    takePictureButton.setOnClickListener(takePictureButtonListener);

    Button addVideoButton = (Button) findViewById(R.id.addVideoButton);
    addVideoButton.setOnClickListener(addVideoButtonListener);

    Button addMusicButton = (Button) findViewById(R.id.addMusicButton);
    addMusicButton.setOnClickListener(addMusicButtonListener);

    Button previewButton = (Button) findViewById(R.id.previewButton);
    previewButton.setOnClickListener(previewButtonListener);

    Button saveButton = (Button) findViewById(R.id.saveButton);
    saveButton.setOnClickListener(saveButtonListener);

    /**
     * Gets the ListView and set its adapter for displaying a list of slideshow
     * items.
     */
    slideshowEditorAdapter = new SlideshowEditorAdapter(this, slideshowInformation.getMediaItemList());
    getListView().setAdapter(slideshowEditorAdapter);
  }

  /**
   * Sets the IDs for each type of media.
   */
  private static final int IMAGE_ID = 1;
  private static final int TAKE_PICTURE_ID = 2;
  private static final int VIDEO_ID = 3;
  private static final int MUSIC_ID = 4;

  /**
   * Called when an Activity returns that was previously launched by this
   * Activity
   */
  @Override
  protected final void onActivityResult(int requestCode, int resultCode, Intent data)
  {
    /**
     * If the Activity executed successfully.
     */
    if (resultCode == RESULT_OK)
    {
      Uri selectedUri = data.getData();

      /**
       * If the Activity returns an image, video or a take picture.
       */
      if (requestCode == IMAGE_ID || requestCode == TAKE_PICTURE_ID || requestCode == VIDEO_ID)
      {
        /**
         * Determines the Media Type.
         */
        MediaItem.MediaType type = (requestCode == VIDEO_ID ? MediaItem.MediaType.VIDEO : MediaItem.MediaType.IMAGE);

        /**
         * Add a new path to the Slideshow.
         */
        slideshowInformation.addMediaItem(type, selectedUri.toString());

        /**
         * Refreshes the ListView.
         */
        slideshowEditorAdapter.notifyDataSetChanged();
      }
      /**
       * If Activity returns a musical track or voice over.
       */
      else
        if (requestCode == MUSIC_ID)
          slideshowInformation.setSlideshowMusicPath(selectedUri.toString());
    }
  }

  /**
   * Called when the user touches the <code>"Add Picture"</code> Button and
   * launches an external image-choosing Activity, such as <code>Gallery</code>
   * or <code>File Explorer</code>.
   */
  private OnClickListener addPictureButtonListener = new OnClickListener()
  {
    /**
     * Launches the Image Choosing activity.
     */
    @Override
    public void onClick(View v)
    {
      /**
       * Creates a new intent with <code>Intent's</code>
       * <code>ACTION_GET_CONTENT</code> constant, which allows the user to
       * select content that is stored on the device.
       */
      Intent intent = new Intent(Intent.ACTION_GET_CONTENT);

      /**
       * Passes a <code>String</code> representing the image <code>MIME</code>
       * type.
       * 
       * <p>
       * The asterisk (*) indicates that any type of image can be selected.
       */
      intent.setType("image/*");

      /**
       * Returns the specified <code>Intent</code>, which displays an Activity
       * chooser that lets the user select which Activity to use for choosing an
       * image, for example, "File Explorer" or "Gallery".
       */
      startActivityForResult(Intent.createChooser(intent, getResources().getText(R.string.choose_image)), IMAGE_ID);
    }
  };

  /**
   * Called when the user touches the <code>"Take Picture"</code> Button, which
   */
  private OnClickListener takePictureButtonListener = new OnClickListener()
  {
    @Override
    public void onClick(View v)
    {
      /**
       * Creates a new <code>Intent</code> to launch the SlideshowTakePicture
       * Activity.
       */
      Intent takePicture = new Intent(SlideshowEditorActivity.this, SlideshowTakePicture.class);
      startActivityForResult(takePicture, TAKE_PICTURE_ID);
    }
  };

  /**
   * Called when the user touches the <code>"Add Video"</code> Button and
   * launches an external video-choosing Activity, such as <code>Gallery</code>
   * or <code>File Explorer</code>.
   */
  private OnClickListener addVideoButtonListener = new OnClickListener()
  {
    @Override
    public void onClick(View v)
    {
      /**
       * Creates a new intent with <code>Intent's</code>
       * <code>ACTION_GET_CONTENT</code> constant, which allows the user to
       * select content that is stored on the device.
       */
      Intent intent = new Intent(Intent.ACTION_GET_CONTENT);

      /**
       * Passes a <code>String</code> representing the video <code>MIME</code>
       * type.
       * 
       * <p>
       * The asterisk (*) indicates that any type of video can be selected.
       */
      intent.setType("video/*");

      /**
       * Returns the specified <code>Intent</code>, which displays an Activity
       * chooser that lets the user select which Activity to use for choosing a
       * video, for example, "File Explorer" or "Gallery".
       */
      startActivityForResult(Intent.createChooser(intent, getResources().getText(R.string.choose_video)), VIDEO_ID);
    }
  };

  /**
   * Called when the user touches the "Add Music" Button.
   */
  private OnClickListener addMusicButtonListener = new OnClickListener()
  {
    @Override
    public void onClick(View v)
    {
      /**
       * Creates a new intent with <code>Intent's</code>
       * <code>ACTION_GET_CONTENT</code> constant, which allows the user to
       * select content that is stored on the device.
       */
      Intent intent = new Intent(Intent.ACTION_GET_CONTENT);

      /**
       * Passes a <code>String</code> representing the audio <code>MIME</code>
       * type.
       * 
       * <p>
       * The asterisk (*) indicates that any type of audio can be selected.
       */
      intent.setType("audio/*");

      /**
       * Returns the specified <code>Intent</code>, which displays an Activity
       * chooser that lets the user select which Activity to use for choosing a
       * musical track or voiceover, for example, "Choose Music Track Explorer"
       * or "File Manager".
       */
      startActivityForResult(Intent.createChooser(intent, getResources().getText(R.string.choose_music)), MUSIC_ID);
    }
  };

  /**
   * Called when the user touches the <code>"Preview"</code> Button which
   * launches the Slideshow Player.
   */
  private OnClickListener previewButtonListener = new OnClickListener()
  {
    @Override
    public void onClick(View v)
    {
      /**
       * Creates a new <code>Intent</code> to launch the Slideshowplayer
       * Activity
       */
      Intent playSlideshow = new Intent(SlideshowEditorActivity.this, SlideshowPlayerActivity.class);

      playSlideshow.putExtra(SlideshowListActivity.NAME_INTENT_EXTRA, slideshowInformation.getName());
      startActivity(playSlideshow);
    }
  };

  /**
   * Called when the user touches the "Save" Button.
   */
  private OnClickListener saveButtonListener = new OnClickListener()
  {
    /**
     * Returns to the previous Activity.
     */
    @Override
    public void onClick(View v)
    {
      finish();
    }
  };

  /**
   * Called when the user touches the "Delete" button next to an ImageView
   * (Thumbnail).
   */
  private OnClickListener deleteButtonListener = new OnClickListener()
  {
    @Override
    public void onClick(View v)
    {
      slideshowEditorAdapter.remove((MediaItem) v.getTag());
    }
  };

  /**
   * Implements the <code>ViewHolder</code> pattern and defines the two GUI
   * components in the ListView.
   * 
   */
  private static class ViewHolder
  {
    ImageView slideImageView;
    Button deleteButton;
  }

  /**
   * Displays the Slideshow items in a ListView.
   */
  private class SlideshowEditorAdapter extends ArrayAdapter<MediaItem>
  {
    private List<MediaItem> items;
    private LayoutInflater inflater;

    public SlideshowEditorAdapter(Context context, List<MediaItem> items)
    {
      super(context, -1, items);
      this.items = items;
      inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
      ViewHolder viewHolder;

      /**
       * If convertView is null, inflate GUI and create ViewHolder.
       */
      if (convertView == null)
      {
        convertView = inflater.inflate(R.layout.activity_slideshow_edit_item, null);

        viewHolder = new ViewHolder();
        viewHolder.slideImageView = (ImageView) convertView.findViewById(R.id.slideshowImageView);
        viewHolder.deleteButton = (Button) convertView.findViewById(R.id.deleteButton);
        convertView.setTag(viewHolder);
      }
      /**
       * Otherwise, get the ViewHolder from the convertView's tag.
       */
      else
        viewHolder = (ViewHolder) convertView.getTag();

      /**
       * Get and display a Thumbnail Bitmap image.
       */
      MediaItem item = items.get(position);
      new LoadThumbnailTask().execute(viewHolder.slideImageView, item.getType(), Uri.parse(item.getPath()));

      /**
       * Configure the "Delete" Button.
       */
      viewHolder.deleteButton.setTag(item);
      viewHolder.deleteButton.setOnClickListener(deleteButtonListener);

      return convertView;
    }
  }

  /**
   * Loads image or video thumbnails in a separate thread.
   */
  private class LoadThumbnailTask extends AsyncTask<Object, Object, Bitmap>
  {
    ImageView imageView;

    /**
     * Loads the thumbnail using ImageView, MediaType and Uri as arguments.
     */
    @Override
    protected Bitmap doInBackground(Object... params)
    {
      imageView = (ImageView) params[0];

      return SlideshowListActivity.getThumbnail((MediaItem.MediaType) params[1], (Uri) params[2], getContentResolver(),
          new BitmapFactory.Options());
    }

    /**
     * Sets the thumbnail on ListView.
     */
    @Override
    protected void onPostExecute(Bitmap result)
    {
      super.onPostExecute(result);
      imageView.setImageBitmap(result);
    }
  }
}