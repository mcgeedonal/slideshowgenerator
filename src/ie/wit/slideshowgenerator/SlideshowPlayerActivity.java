package ie.wit.slideshowgenerator;

import java.io.FileNotFoundException;
import java.io.InputStream;

import android.app.Activity;
import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.VideoView;

/**
 * Plays the selected slideshow with an optional musical or voiceover track.
 * 
 * <p>
 * Author: Donal McGee
 */

public class SlideshowPlayerActivity extends Activity
{
  private static final String TAG = "SLIDESHOW";

  /**
   * Constants for saving the slideshow state when its configuration changes.
   */

  private static final String MEDIA_TIME = "MEDIA_TIME";
  private static final String IMAGE_INDEX = "IMAGE_INDEX";
  private static final String SLIDESHOW_NAME = "SLIDESHOW_NAME";
  private static final int DURATION = 5000;
  private ImageView imageView;
  private VideoView videoView;
  private String slideshowName;
  private SlideshowInformation slideshowInformation;
  private BitmapFactory.Options options;
  private Handler handler;
  private int nextItemIndex;
  private int mediaTime;
  private MediaPlayer mediaPlayer;

  /**
   * Initializes the SlideshowPlayer Activity.
   */
  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_slideshow_player);

    imageView = (ImageView) findViewById(R.id.slideshowImageView);
    videoView = (VideoView) findViewById(R.id.slideshowVideoView);

    videoView.setOnCompletionListener(new OnCompletionListener()
    {
      @Override
      public void onCompletion(MediaPlayer mp)
      {
        handler.post(updateSlideshow);
      }
    });

    /**
     * If the Activity is starting: get the slideshow name from the Intent's
     * extras.
     */
    if (savedInstanceState == null)
    {
      slideshowName = getIntent().getStringExtra(SlideshowListActivity.NAME_INTENT_EXTRA);
      mediaTime = 0;
      nextItemIndex = 0;
    }

    /**
     * If the Activity is resuming: get the saved information when the slideshow
     * configuration was changed.
     */
    else
    {
      mediaTime = savedInstanceState.getInt(MEDIA_TIME);
      nextItemIndex = savedInstanceState.getInt(IMAGE_INDEX);
      slideshowName = savedInstanceState.getString(SLIDESHOW_NAME);
    }

    /**
     * Gets the slideshowInformation for the slideshow.
     */
    slideshowInformation = SlideshowListActivity.getSlideshowInformation(slideshowName);

    /**
     * Configures the BitmapFactory.Options for loading images.
     */
    options = new BitmapFactory.Options();
    options.inSampleSize = 1;

    /**
     * If there is music or a voiceover to play: try to create a MediaPlayer to
     * play the music
     */
    if (slideshowInformation.getSlideshowMusicPath() != null)
    {
      try
      {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setDataSource(this, Uri.parse(slideshowInformation.getSlideshowMusicPath()));
        mediaPlayer.prepare();
        mediaPlayer.setLooping(true); //
        mediaPlayer.seekTo(mediaTime);
      }

      catch (Exception e)
      {
        Log.v(TAG, e.toString());
      }
    }

    handler = new Handler();
  }

  /**
   * Posts the updateSlideshow Runnable for execution.
   */
  @Override
  protected void onStart()
  {
    super.onStart();
    handler.post(updateSlideshow);
  }

  /**
   * Called when the Activity is paused.
   */
  @Override
  protected void onPause()
  {
    super.onPause();

    if (mediaPlayer != null)
      mediaPlayer.pause();
  }

  /**
   * Called after onStart or onPause.
   */
  @Override
  protected void onResume()
  {
    super.onResume();

    if (mediaPlayer != null)
      mediaPlayer.start(); // Resume playback
  }

  /**
   * Called when the Activity stops.
   * 
   * <p>
   * Uses a handler to prevent a slideshow from operating when in background.
   */
  @Override
  protected void onStop()
  {
    super.onStop();
    handler.removeCallbacks(updateSlideshow);
  }

  /**
   * Called when the Activity is destroyed.
   * 
   * <p>
   * Releases MediaPlayer resources.
   */
  @Override
  protected void onDestroy()
  {
    super.onDestroy();

    if (mediaPlayer != null)
      mediaPlayer.release();
  }

  /**
   * Saves the slideshow state so it can be restored in onCreate.
   */
  @Override
  protected void onSaveInstanceState(Bundle outState)
  {
    super.onSaveInstanceState(outState);

    /**
     * If there is a mediaPlayer, then store the media's current position.
     */
    if (mediaPlayer != null)

      outState.putInt(MEDIA_TIME, mediaPlayer.getCurrentPosition());

    outState.putInt(IMAGE_INDEX, nextItemIndex - 1);
    outState.putString(SLIDESHOW_NAME, slideshowName);
  }

  /**
   * Implements Runnable to control a slideshow.
   */
  private Runnable updateSlideshow = new Runnable()
  {
    @Override
    public void run()
    {
      if (nextItemIndex >= slideshowInformation.size())
      {
        /**
         * If there is music playing when the slideshow ends, reset the
         * MediaPlayer.
         */
        if (mediaPlayer != null && mediaPlayer.isPlaying())
          mediaPlayer.reset();
        finish();
      }

      else
      {
        MediaItem item = slideshowInformation.getMediaItemAt(nextItemIndex);

        if (item.getType() == MediaItem.MediaType.IMAGE)
        {
          imageView.setVisibility(View.VISIBLE);
          videoView.setVisibility(View.INVISIBLE);
          new LoadImageTask().execute(Uri.parse(item.getPath()));
        }

        else
        {
          imageView.setVisibility(View.INVISIBLE);
          videoView.setVisibility(View.VISIBLE);
          playVideo(Uri.parse(item.getPath()));
        }

        ++nextItemIndex;
      }
    }

    /**
     * Loads the images and videos and transitions from one to the next.
     */
    class LoadImageTask extends AsyncTask<Uri, Object, Bitmap>
    {
      @Override
      protected Bitmap doInBackground(Uri... params)
      {
        return getBitmap(params[0], getContentResolver(), options);
      }

      /**
       * Sets the thumbnail on ListView.
       */
      @Override
      protected void onPostExecute(Bitmap result)
      {
        super.onPostExecute(result);
        @SuppressWarnings("deprecation")
        BitmapDrawable nextDrawable = new BitmapDrawable(result);
        nextDrawable.setGravity(android.view.Gravity.CENTER);
        Drawable previousDrawable = imageView.getDrawable();

        /**
         * If previous is a TransitionDrawable, its second Drawable item is
         * gotten.
         */
        if (previousDrawable instanceof TransitionDrawable)
          previousDrawable = ((TransitionDrawable) previousDrawable).getDrawable(1);

        if (previousDrawable == null)
          imageView.setImageDrawable(nextDrawable);
        else
        {
          Drawable[] drawables = { previousDrawable, nextDrawable };
          TransitionDrawable transition = new TransitionDrawable(drawables);
          imageView.setImageDrawable(transition);
          transition.startTransition(1000);
        }

        handler.postDelayed(updateSlideshow, DURATION);
      }
    }

    /**
     * Utility method to get a Bitmap from a Uri
     * 
     * <p>
     * @param uri
     * @param cr
     * @param options
     * @return bitmap
     */
    public Bitmap getBitmap(Uri uri, ContentResolver cr, BitmapFactory.Options options)
    {
      Bitmap bitmap = null;

      try
      {
        InputStream input = cr.openInputStream(uri);
        bitmap = BitmapFactory.decodeStream(input, null, options);
      }
      catch (FileNotFoundException e)
      {
        Log.v(TAG, e.toString());
      }

      return bitmap;
    }

    /**
     * Configures the video and plays it.
     * 
     * @param videoUri
     */
    private void playVideo(Uri videoUri)
    {
      videoView.setVideoURI(videoUri);
      videoView.setMediaController(new MediaController(SlideshowPlayerActivity.this));
      videoView.start();
    }
  };
}